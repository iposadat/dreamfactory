FROM dreamfactorysoftware/df-docker

#fixups to allow apache to run as non-root
# - allow root group write to lock and run dirs, and dreamfactory directory
# - remove SERVERNAME-related lines from /etc/apache2/sites-available/dreamfactory.conf and entrypoint
# - change HTTP ports to 8080
# - log to /tmp
#RUN chmod -R g+w /var/lock/apache2 /var/run/apache2 /opt/dreamfactory && \
#    sed -i "/SERVERNAME/d" /docker-entrypoint.sh /etc/apache2/sites-available/dreamfactory.conf && \
#    sed -i "s/\b80\b/8080/g" /etc/apache2/sites-available/dreamfactory.conf /etc/apache2/ports.conf && \
#    sed -i "s;APACHE_LOG_DIR=.*;APACHE_LOG_DIR=/tmp;g" /etc/apache2/envvars

RUN chmod -R g+w /etc/ssmtp/

#change exposed port to 8080
EXPOSE 8080