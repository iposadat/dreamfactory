# Dreamfactory
Docker container for DreamFactory API

## Prerequisites
- Download the openshift CLI: [https://docs.openshift.org/latest/cli_reference/get_started_cli.html](https://docs.openshift.org/latest/cli_reference/get_started_cli.html).
- Log in: `oc login`
- Create new project on openshift origin: `oc new-project nameOfProject`

## Configuration method 1 (use Docker Hub Image)
- Run application setting proper parameters:

`oc new-app https://gitlab.cern.ch/iposadat/dreamfactory.git --name=dreamfactory`
- Once pushed, set proper env variables:

`oc env dc/dreamfactory DB_DRIVER=mysql DB_HOST=db-50100.cern.ch DB_DATABASE=dreamfactory DB_USERNAME=**** DB_PASSWORD=**** DB_PORT=5505 REDIS_HOST=rd REDIS_DATABASE=0 REDIS_PORT=6379`
- Finally, expose application once it is deployed:

`oc expose svc dreamfactory`

This image is based on the original one allowing non-root users to run it.

To see what's **going** on, run: `oc status`